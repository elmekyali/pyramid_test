package com.nespresso.pyramid.layers.layer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LayerFactory {
    private static final Pattern LAYER_PATTERN = Pattern.compile("^(\\d+) Slaves, (\\d+) Anks$");
    private static final int SLAVES_PER_BLOCK = 50;
    private static final int HIGH_QUALITY_BLOCK_COST = 2;

    public Layer createFrom(String layerDescription) {
        Matcher matcher = LAYER_PATTERN.matcher(layerDescription);
        if (matcher.find()) {
            int slaves = Integer.parseInt(matcher.group(1));
            int anks = Integer.parseInt(matcher.group(2));
            int nbOfBlocks = slaves / SLAVES_PER_BLOCK;
            int anksPerBlock = anks / nbOfBlocks;

            if (anksPerBlock >= HIGH_QUALITY_BLOCK_COST) {
                return new HighQualityLayer(nbOfBlocks);
            } else {
                return new LowQualityLayer(nbOfBlocks);
            }
        }
        return new NullLayer();
    }
}
