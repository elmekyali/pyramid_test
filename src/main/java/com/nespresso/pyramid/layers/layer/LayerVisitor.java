package com.nespresso.pyramid.layers.layer;

public interface LayerVisitor {
    void visit(int size, String blockRepresentation);
}
