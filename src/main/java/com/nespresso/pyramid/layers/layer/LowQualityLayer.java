package com.nespresso.pyramid.layers.layer;

public class LowQualityLayer extends Layer {
    public LowQualityLayer(int size) {
        super(size);
        this.blockRepresentation = "V";
    }

    @Override
    public boolean willCollapse(Layer layer) {
        if (size > layer.size) {
            return true;
        }
        if (size == layer.size) {
            return getClass().equals(layer.getClass());
        }
        return false;
    }
}
