package com.nespresso.pyramid.layers.layer;

public class NullLayer extends Layer {
    public NullLayer(int size) {
        super(0);
        this.blockRepresentation = "";
    }

    public NullLayer() {
        this(0);
    }

    @Override
    public boolean willCollapse(Layer layer) {
        return false;
    }
}
