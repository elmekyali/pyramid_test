package com.nespresso.pyramid.layers.layer;

public abstract class Layer {
    protected final int size;
    protected String blockRepresentation;

    protected Layer(int size) {
        this.size = size;
    }

    public abstract boolean willCollapse(Layer layer);

    public void accept(LayerVisitor visitor){
        visitor.visit(size,blockRepresentation);
    }
}
