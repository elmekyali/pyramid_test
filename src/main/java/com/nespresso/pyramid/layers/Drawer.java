package com.nespresso.pyramid.layers;

public interface Drawer {
    String draw();
}
