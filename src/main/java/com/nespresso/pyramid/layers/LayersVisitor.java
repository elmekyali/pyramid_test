package com.nespresso.pyramid.layers;

import com.nespresso.pyramid.layers.layer.Layer;

public interface LayersVisitor {
    void visit(Layer next);
}
