package com.nespresso.pyramid;

import com.nespresso.pyramid.layers.PyramidDrawer;
import com.nespresso.pyramid.layers.layer.LayerDrawer;
import com.nespresso.pyramid.layers.layer.LayerFactory;
import com.nespresso.pyramid.layers.Layers;

public class Pyramid {
    private final LayerFactory layerFactory;
    private final Layers layers;

    Pyramid() {
        this.layerFactory = new LayerFactory();
        this.layers = new Layers();
    }

    public void addLayer(String layerDescription) {
        this.layers.add(layerFactory.createFrom(layerDescription));
    }

    public String print() {
        PyramidDrawer pyramidDrawer = new PyramidDrawer(new LayerDrawer());
        layers.accept(pyramidDrawer);

        return pyramidDrawer.draw();
    }
}
